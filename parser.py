
def getPosition(spl_data):
    return spl_data[2].split('.')[0].strip()

def getName(spl_data):
    return spl_data[3].split('<')[0].strip()

def getNationality(spl_data):
    temp = spl_data[4][6:32].strip()
    return temp

def getBirthYear(spl_data):
    return spl_data[4][1:5].strip()

def getTeam(spl_data):
    return spl_data[4][32:64].strip()

def getTime(spl_data):
    # '2:35.48,6'
    string_time = spl_data[4][65:74].strip()
    hours = string_time.split(':')[0]
    minutes = string_time.split(':')[1].split('.')[0]

    return int(hours) * 60 + int(minutes)


with open('zueri_marathon_2016.html') as myFile:
    data = myFile.readlines()[68:6114]



#print(data)

relevantData = list()
position = list()
name = list()
birthYear = list()
time = list()
team = list()
nationality = list()



for x in range(0, len(data), 3):
    relevantData.append(data[x])
    splitted_data = data[x].split('>')
    #print(splitted_data)
    position.append(getPosition(splitted_data))
    name.append(getName(splitted_data))
    birthYear.append(getBirthYear(splitted_data))
    nationality.append(getNationality(splitted_data))
    time.append(getTime(splitted_data))
    team.append(getTeam(splitted_data))

with open('output.csv', 'w+') as output:
    for x in range(0, len(name)):
        output.write('{},{},{}\n'.format(position[x], name[x], str(time[x])))


#print(relevantData)