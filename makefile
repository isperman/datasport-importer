OSTYPE := $(shell uname)
$(info $$OSTYPE is [${OSTYPE}])

ifeq ($(OSTYPE),Msys)
	MAKE = mingw32-make
else
	MAKE = make
endif

MAIN = main

.PHONY: all

default: doit

doit:
	latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode -synctex=1" -use-make $(MAIN).tex
	$(MAKE) -j4 -f $(MAIN).makefile
ifeq ($(OSTYPE),Msys)
	latexmk -CA # argh! needed to work on Windows
endif

	latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode -synctex=1" -use-make $(MAIN).tex

clean:
	latexmk -CA
